# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        
        ret_list = ListNode()
        current = ret_list
        
        while list1 or list2:
            
            if list1 and not list2:
                next_val = list1.val
                list1 = list1.next
            elif list2 and not list1:
                next_val = list2.val
                list2 = list2.next
            else:
                if list1.val <= list2.val:
                    next_val = list1.val
                    list1 = list1.next
                else:
                    next_val = list2.val
                    list2 = list2.next
            current.next = ListNode(next_val)
            current = current.next
            
        return ret_list.next