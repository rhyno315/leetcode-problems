class Solution:
    def pivotIndex(self, nums: list[int]) -> int:
        if len(nums) == 0:
            return -1
        l_total = 0
        r_total = sum(nums)
        for i in range(len(nums)):
            r_total -= nums[i]
            if l_total == r_total:
                return i
            l_total += nums[i]
        return -1