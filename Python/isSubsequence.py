class Solution:
    def isSubsequence(self, s: str, t: str) -> bool:
        if len(s) == 0:
            return True
        elif len(t) == 0:
            return False
        elif len(s) == 1 and s in t:
            return True
        else:
            i = 0
            for j in range(0, len(t)):
                if(t[j]==s[i]):
                    i += 1
                    if i == len(s):
                        return True

            if i != len(s):
                return False
            else:
                return True