class Solution:
    def runningSum(self, nums: list[int]) -> list[int]:
        
        ret_nums = list()
        ret_nums.append(nums[0])
        for i in range(1, len(nums)):
            ret_nums.append(ret_nums[i-1] + nums[i])
        return ret_nums