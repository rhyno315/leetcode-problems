int pivotIndex(int* nums, int numsSize){
    if(nums == NULL){
        return -1;
    }
    
    size_t size = sizeof(nums) / sizeof(int);
    int left_sum = 0;
    int right_sum = 0;
    int i = 0;
    for (; i < numsSize; i++) {
        right_sum += nums[i];
    }
    
    for (i = 0; i< numsSize; i++) {
        right_sum -= nums[i];
        if (right_sum == left_sum) {
            return i;
        }
        left_sum += nums[i];
    }
    
    return -1;
}
