/*
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* twoSum(int* nums, int numsSize, int target, int* returnSize)
{

    int* retVal = (int*)calloc(2, sizeof(int));
    for(int i = 0; i < numsSize; i++) 
    {
        for (int j = i + 1; j < numsSize; j++ )
        {
            if(nums[j] == target - nums[i] )
            {
            retVal[0] = i;
            retVal[1] = j;
            *returnSize = 2;
            return retVal;
            } 
        }
    }
    return returnSize;
}