bool isSubsequence(char * s, char * t){
    int i;
    bool ret;
    size_t s_len = strlen(s);
    size_t t_len = strlen(t);
    
    if (s_len == 0) {
        return true;
    }
    else if (strcmp(s,t) == 0) {
        return true;
    }
    else if (t_len == 0) {
        return false;
    }
    else if (strcmp(s,t) != 0 && t_len == 1) {
        return false;
    }
    else if (s_len == 1) 
    {   
        for(i = 0; i < t_len; i++) 
        {
            if (t[i] == s[0]) {
                return true;
            }
        }
    }
    else 
    {
        i = 0;
        for (int j = 0; j < t_len; j++) {
            if (t[j] == s[i]) {
                i++;
                if (i == s_len) {
                    return true;
                }
            }
        }
    }
    if (i != s_len) {
        return false;
    }
    return true;
}