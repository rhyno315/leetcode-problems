/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* runningSum(int* nums, int numsSize, int* returnSize){
    if (nums == NULL) {
        return nums;
    }
    int* ret_val;
    ret_val = (int*)malloc(numsSize * sizeof(int));
    int i = 1;
    ret_val[0] = nums[0];
    for (; i < numsSize; i++) {
        ret_val[i] = ret_val[i-1] + nums[i];  
    }
       *returnSize = i;
        return ret_val;
}